<!DOCTYPE html>
<html>
	<head>
		<title>Challenge 4.2</title>
		<link rel="stylesheet" type="text/css" href="styles.css">
	</head>
	<body>
			<?php
				$form_block = <<<HERE
				<form method="POST" action="$_SERVER[PHP_SELF]">
				
				<p><strong>First Name:</strong><br/>
				<input type="text" name="first_name" value="$_POST[first_name]" size=30 required/></p>
				
				<p><strong>Last Name:</strong><br/>
				<input type="text" name="last_name" value="$_POST[last_name]" size=30 required/></p>
				
				<p><strong>Year of Birth</strong><br/>
				<input type="number" name="dob" value="$_POST[dob]" min="1913" max="2013" required/></p>
				
				<p><strong>Current grade(10, 11, or 12)</strong><br/>
				<input type="number" name="grade" value="$_POST[grade]" min="10" max="12" required/></p>
				
				<p><strong>Number of siblings</strong><br/>
				<input type="number" name="siblings" value="$_POST[siblings]" min="0"/></p>
				
				<p><strong>Bedtime and time you wake up</strong><br/>
				<input type="time" name="bedtime" value="$_POST[bedtime]" required/><br/><input type="time" name="waketime" value="$_POST[waketime]" required/></p>
				
				<p><strong>All times for the rest of the questions are in hours per day in an average 7 day week (e.g. 3 hours per day)</strong></p>
				
				<p><strong>Time spent on homework</strong><br/>
				<input type="number" name="homework" value="$_POST[homework]" min="0" max="24" required/></p>
				
				<p><strong>Time spent watching TV and or films</strong><br/>
				<input type="number" name="tv_dvd" value="$_POST[tv_dvd]" min="0" max="24" required/></p>
				
				<p><strong>Time spent using computer or game consoles</strong><br/>
				<input type="number" name="comp_console" value="$_POST[comp_console]" min="0" max="24" required/></p>
				
				<p><strong>Time spent with family</strong><br/>
				<input type="number" name="family" value="$_POST[family]" min="0" max="24"/></p>
				
				<p><strong>Time spent with friends</strong><br/>
				<input type="number" name="friends" value="$_POST[friends]" min="0" max="24"/></p>
				
				<input type="hidden" name="op" value="ds"/>
				
				<p><input type="submit" name="submit" value="Send This Form"/></p>
				</form>
HERE;

				$timeStamp = date("Y-m-d H:i:s");
				
				if($_POST['op']!="ds"){
					echo"$form_block";
				}else if ($_POST['op']=="ds"){
					// check value of $_POST[first_name] and $_POST[last_name]
					if($_POST['first_name'] == "" || $_POST['last_name'] == ""){
						$name_err = "<h4>Please enter your name!</h4><br/>";
						$send = "no";
					}
					
					// check value of $_POST[dob]
					if($_POST['dob']=="") {
						$dob_err = "<h4>Please enter a Year of Birth!</h4><br/>";
						$send = "no";
					}
					
					// check value of $_POST[siblings]
					if($_POST['siblings']=="") {
						$_POST['siblings'] = 0;
					}
					
					// check value of $_POST[bedtime]
					if($_POST['bedtime']=="" || $_POST['waketime']=="") {
						$sleep_err = "<h4>Please enter a bed and/or wake time!</h4><br/>";
						$send = "no";
					}
					
					// check value of $_POST[homework]
					if($_POST['homework']=="" || $_POST['tv_dvd']=="" || $_POST['comp_console']=="") {
						$time_err = "<h4>Please enter Time Spent doing activities!</h4><br/>";
						$send = "no";
					}
					
					if($send != "no") {
						// it's ok to send, so build the mail
						
						if($_POST['grade'] == 10)$school_left = 3;
						else if($_POST['grade'] == 11)$school_left = 2;
						else if($_POST['grade'] == 12)$school_left = 1;
						else $school_left = null;
						
						$homework_time = $_POST['homework']*7*$school_left*40;
						$screen_time = ($_POST['tv_dvd'] + $_POST['comp_console'])*7*$school_left*52;
						
						$time_awake = (24 -((24 - $_POST['bedtime']) + $_POST['waketime']))*7*$school_left*52;
						
						$screen_percent = floor(($screen_time/$time_awake)*100);
						
						$results_block = <<<HERE
				<table>
					<tr>
						<th>Name</th>
						<td>$_POST[first_name] $_POST[last_name]</td>
					</tr>
					<tr>
						<th>Year of Birth</th>
						<td>$_POST[dob]</td>
					</tr>
					<tr>
						<th>Current Grade</th>
						<td>$_POST[grade]</td>
					</tr>
					<tr>
						<th>Number of siblings</th>
						<td>$_POST[siblings]</td>
					</tr>
					<tr>
						<th>Time Spent doing homework<br/> till the end of grade 12</th>
						<td>$homework_time hours</td>
						
					</tr>
					<tr>
						<th>Time Spent in front of a screen<br/> till the end of grade 12</th>
						<td>$screen_time hours</td>
					</tr>
					<tr>
						<th>Percentage of time awake<br/> spent in front of a screen</th>
						<td>$screen_percent %</td>
					</tr>
				</table>
HERE;
						
						$msg=<<<HERE
						<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
						<html>
							<head>
								<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
								<style type="text/css">
									table{
									border-collapse:collapse;
									}
									
									table, td, th{
									border:1px solid black;
									}
									
									td{
									text-align:center;
									}
								</style>
							</head>
							<body>
								E-MAIL SENT FROM The Parents and Community committee<br/>
								$results_block<br/>
								Message sent at $timeStamp<br/><br/>
							</body>
						</html>
HERE;
						
						$to = "macintose@gmail.com";
						$subject = "$_POST[first_name] $_POST[last_name]'s Survey results";
						$mailheader = "From:The Parents and Community committee <webmaster@nscctruro.ca>\n";
						//send mail
						mail($to, $subject, $msg, $mailheader);
						//display configuration to user
						echo "<p>Mail has been sent!</p>";
						echo "<p>$mailheader</p>";
						echo "<p>Great! Thanks $_POST[first_name] for responding to our survey<br/></p>";
						echo "<p>$results_block</p>";
					} else if ($send == "no") {
						echo "$name_err";
						echo "$dob_err";
						echo "$sleep_err";
						echo "$time_err";
						echo "$form_block";
					}
				}
			?>
	</body>
</html>